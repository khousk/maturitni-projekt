<?php

require_once 'lib/limonade.php';
$db = new PDO("sqlite:db/db.sqlite");

function configure() {

   $env =  ENV_DEVELOPMENT; #ENV_PRODUCTION;
   option('env', $env);
# spustí se na začátku
}

function before() {
    # spustí se před načtením webu, získá globální proměnné ze souboru .json
    layout('layouts/standard.html.php');
    error_layout('lib/limonade/views/default_layout.html.php');

    $str_data = file_get_contents('data/styleconfiguration.json');
    $data = json_decode($str_data, true);

    set('STYL', 'styles/' . $data["styl"] . '.css');
    set('NAVSTYL', 'styles/' . $data["navstyl"] . '.css');
    set('NAME', $data["schoolname"]);
    set('USERNAME', "");

    set('NAZEVSTRANKY', 'Úvod');
    set('STRANKA', 'Neoficiální');
    # Je použito jako titulek pro všechny stránky, které nejsou speciální pro danou školu

    set('STREET', $data["street"]);
    set('CITY', $data["city"]);
    set('ZIP', $data["zip"]);
    set('REGION', $data["region"]);
    set('BAKALARI_ODKAZ', $data["bakalariOdkaz"]);
    set('ROZVRH_NAZEV_SOUB', $data["rozvrh"]);

    set('LOGOSRC', 'pictures/' . $data["logoSkoly"]);
    set('POZADISRC', 'pictures/' . $data["pozadi"] . '.svg');
    set('ZRIZOVATEL', 'pictures/' . $data["zrizovatel"]);

   $_SESSION["isLoggedIn"] = false;


    # Nastavení všech proměnných, které budou používané na různých stránkách blogu. Většina z nich ovlivňuje vzhled finální stránky.

}

dispatch ('/', 'homepage');
function homepage () {
    if ($_SESSION["isLoggedIn"]) {

        return html('Vítejte');
    }
   return html('oskole.html');
# Základní stránka
}

dispatch_POST ('/', 'login');
function login () {
    global $db;
    $username = $_POST["jmeno"];
    $password = $_POST["heslo"];

    foreach ($db->query("SELECT username, password FROM users") as $row) {
        if ($row["username"] == $username) {
            if ($row["password"] == $password) {
                $_SESSION["isLoggedIn"] = true;
                return html('Podařilo se Vám přihlásit pod jménem ' . $username);

            }
        }
    }
    return html('Nepodařilo se Vám přihlásit - zkuste jiné přihlašovací jméno nebo heslo.');
# Přihlašování - zjišťuje zda zadaná dvojice jméno a heslo odpovídá některé dvojici v databázi
}

dispatch_POST ('/odhlaseni', 'logout');
function logout () {
    $_SESSION["isLoggedIn"] = false;
    return html("Byl jste odhlášen.");
# Odhlašování
}

dispatch ('/setup', 'setup');
function setup (){
    return html('setup.html.php');
# stránka, kde se nastavují všechny informace o blogu - CSS styl, jména a zanoření stránek, o které přednastavené stránky má uživatel zájem
}

dispatch_post ('/setup', 'setup_change');
function setup_change (){
    $str_data = file_get_contents('data/styleconfiguration.json');
    $styleData = json_decode($str_data, true);

    if ($_POST["styl"] == 1) {
        set('STYL', 'styles/zakladStylu.css');
        $styleData["styl"] = "zakladStylu";
    }
    else if ($_POST["styl"] == 2) {
        set('STYL', 'styles/stylDva.css');
        $styleData["styl"] = "stylDva";
    }
    else if ($_POST["styl"] == 3) {
        set('STYL', 'styles/stylTri.css');
        $styleData["styl"] = "stylTri";
    }
    else {
        set('STYL', 'styles/stylCtyri.css');
        $styleData["styl"] = "stylCtyri";
    }

    $fh = fopen('data/styleconfiguration.json', 'w');
    fwrite($fh, json_encode($styleData, JSON_UNESCAPED_UNICODE));
    fclose($fh);

    return html('setup_change.html.php');

# stránka, kde se nastavuje CSS styl blogu - Vaše volba se automaticky zapíše do stylesconfiguration.json
}
dispatch ('/download', 'download');
function download (){
    return html('stahni.html.php');
# Stáhne zip soubor s tímto frameworkem pro SŠ
}

dispatch ('/clanek', 'novyClanek');
function novyClanek (){
    return html('novyClanek.html.php');
# zde se dají vytvořit nové články, které pak vložíme do databáze
}
dispatch_post ('/clanek', 'ulozeniClanku');
function ulozeniClanku (){

    global $db;
    $obsah = $_POST["obsah"];
    $title = $_POST["title"];
 #    $autor = $_SESSION["username"];
    $adresa = $_POST["adresa"];
    $zanoreni = $_POST["zanoreni"];

    $db->query("INSERT INTO 'articles'('obsah', 'autor', 'datum', 'title', 'adresa', 'zanoreni') VALUES('$obsah', 'autor', CURRENT_TIMESTAMP, '$title', '$adresa', '$zanoreni')");

    return html('Podařilo se!');

# Tady se ukládají všechny informace o novém článku. Pozor - autor se může rozhodnout zůstat anonymní
}
dispatch ('/update', 'update');
function update (){
    return html('update_clanek.html.php');
# Zde se dají updatovat články, které již existují. Pozor - změní se všechny dosavadní informace o článku - co do něj teď znovu nezadáte zmizí navždy.
}
dispatch_post ('/update', 'updateClanku');
function updateClanku (){

    global $db;
    $obsah = $_POST["obsah"];
    $title = $_POST["title"];
   # $autor = $_SESSION["username"];
    $adresa = $_POST["adresa"];
    $zanoreni = $_POST["zanoreni"];


    $db->query("INSERT INTO 'articles' SET('$obsah', 'autor', CURRENT_TIMESTAMP, '$title', '$adresa', '$zanoreni') WHERE adresa = '$adresa' ");
# vloží data do databáze

    return html('Nově zadané informace jsme úspěšně uložili');

# Tady se ukládají všechny informace o nově změněném článku. Pozor - vymění se kompletně všechny dosavadní informace o článku.
}
dispatch ('/delete', 'delete');
function delete (){
    return html('delete_clanek.html.php');
# Zde se dají updatovat články, které již existují. Pozor - změní se všechny dosavadní informace o článku - co do něj teď znovu nezadáte zmizí navždy.
}
dispatch_post ('/delete', 'smazano');
function smazano (){

    global $db;
    $adresa = $_POST["adresa"];
    $db->query(" DELETE FROM articles WHERE adresa = '$adresa' ");
    #vloží data do databáze

    return html('Úspěšně jsme smazali stránku');

# Tady se ukládají všechny informace o nově změněném článku. Pozor - vymění se kompletně všechny dosavadní informace o článku.
}

dispatch ('/rozvrh', 'rozvrh');
function rozvrh (){
    return html('rozvrh.html.php');
# Zde se dají updatovat články, které již existují. Pozor - změní se všechny dosavadní informace o článku - co do něj teď znovu nezadáte zmizí navždy.

}
dispatch ('/odkaz', 'tvorbaOdkazu');
function tvorbaOdkazu (){
    return html('<h3>Vložení odkazu</h3>
<form method="POST" action="index.php?/odkaz">
  <label for="name">Název odkazu:</label><br>
  <input type="text" name="name" autocomplete="on"><br>

  <label for="odkaz">Internetová adresa odkazu:</label><br>
  <input type="text" name="odkaz" autocomplete="on"><br>

  Před zadáním se ujistěte, že jste vyplnili všechna políčka.
  <br><input type="submit" name="submit" /><br>

</form>');
# Zde se dají updatovat články, které již existují. Pozor - změní se všechny dosavadní informace o článku - co do něj teď znovu nezadáte zmizí navždy.

}

dispatch_post ('/odkaz', 'odkaz');
function odkaz (){
    return html('tvorbaOdkazu.html.php');
# Pomáhá vytvořit odkaz, který pak správce webu může zakomponovat do svého článku.
}
dispatch ('/maturita', 'maturita');
function maturita (){
    return html('maturita.html.php');
# Zde se dají updatovat články, které již existují. Pozor - změní se všechny dosavadní informace o článku - co do něj teď znovu nezadáte zmizí navždy.
}
dispatch_post ('/maturita', 'predmety');
function predmety (){
    return html('vyberPredmetu.php');


}

dispatch('/stranka/:STR', 'stranky');
function stranky() {
    set('STR', params("STR"));
    return html("stranka.html.php");
# Do adresy, která se objeví na stránce nesmíte dávat dvojtečku, nefungovalo by pro takovou stránku vyhledávání obsahu.

}


dispatch('/problemySPrihlasenim', 'problem');
function problem() {
    return html("Obraťte se na správce webu.");
}

run();

?>