<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles/zakladStylu.css" />
    <link rel="stylesheet" href="styles/menuVlevo.css" />
	<title>Framework pro stránky SŠ</title>

</head>
<body>

  <div id="wrapper-header">
    <h1>Framework pro stránky SŠ</h1>
  </div>
  
  <div id="wrapper-obsah">
    <?php echo error_notices_render(); ?>
    <?php echo $content;?>
    <div>

    </div>
  </div>

</body>
</html>
