<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="<?php echo $STYL; ?>" />
        <link rel="stylesheet" href="<?php echo $NAVSTYL; ?>" />
        <title><?php echo $NAME; echo " - "; echo $STRANKA; ?></title>
    </head>
    <body>
        <div id="wrapper-header">
            <?php include ("header.php");?>
        </div>
          <br>
          <?php include ("menu.php");?>
        <div id="wrapper-obsah">
            <?php echo $content; ?>
        </div>
        <?php include ("footer.php");?>
    </body>
</html>